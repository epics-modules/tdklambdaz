tdklambdaz
======
European Spallation Source ERIC Site-specific EPICS module : tdklambdaz

This module provides device support for the TDK Lambda Z-series family of programmable DC power supplies. The device support is based on the LAN interface with daisy chaining via the RS485 interface

- Manufacturer page: [link](https://product.tdk.com/en/search/power/switching-power/prg-power/list#type=Z%2B&psts%5B%5D=0&_l=50&_p=1&_c=part_no-part_no&_d=0)
- User manual: [Zplus_HV_Manuel_E.pdf](docs/Zplus_HV_Manuel_E.pdf)
- LAN Interface User Manual: [zplus_lan_manual](docs/zplus_lan_manual.pdf)

Dependencies
============

*  `stream`


Using the module
================
In order to use the device support module declare a require statement for streamdevice and tdklambdaz:

```
    require stream
    require tdklambdaz
```

Then load the database for the master device (`TS2-010CRM:Cryo-PSU-010`):

```
    iocshLoad("$(tdklambdaz_DIR)/tdklambdaz_master.iocsh", "DEVICENAME=TS2-010CRM:Cryo-PSU-010, IPADDR=ts2-cryo-tdklambda1")
```

Parameters:
- DEVICENAME: The name of the device as registered in the Naming service
- IPADDR: The IP address of your power supply LAN module
- RS485_ADDR (optional): The RS-485 address of this device (used for daisy chaining). Default: 1

Then load the database for a slave device (`TS2-010CRM:Cryo-PSU-011`):

```
    iocshLoad("$(tdklambdaz_DIR)/tdklambdaz_slave.iocsh", "DEVICENAME=TS2-010CRM:Cryo-PSU-011, MASTER=TS2-010CRM:Cryo-PSU-010, RS485_ADDR=2")
```

Parameters:
- DEVICENAME: The name of the device as registered in the Naming service
- MASTER: The devicename of the master device with the LAN module
- RS485_ADDR:  The RS-485 address of this device (used for daisy chaining).


Building the module
===================
This module should compile as a regular EPICS module.

* Run `make` from the command line.
