/**
 * @brief Returns string depending on the error code.
 */

#include <string.h>
// Subroutine
#include <aSubRecord.h>
// Export Subroutine Function
#include <registryFunction.h>
#include <epicsExport.h>


static long tdklambdaz_get_error_msg(aSubRecord *prec)
{
  const char *errmsg = NULL;
  switch(*((long*) prec->a))
  {
    case 0:    errmsg = "No error";
               break;

    case -100: errmsg = "Command error";
               break;

    case -101: errmsg = "Invalid Character";
               break;

    case -102: errmsg = "Syntax error";
               break;

    case -104: errmsg = "Data type error";
               break;

    case -109: errmsg = "Missing parameter";
               break;

    case -112: errmsg = "Program word too long";
               break;

    case -131: errmsg = "Invalid Suffix";
               break;

    case -222: errmsg = "Data out of range";
               break;

    case -241: errmsg = "Hardware Missing";
               break;

    case -350: errmsg = "Queue Overflow";
               break;

    case 300:  errmsg = "Execution error";
               break;

    case 301:  errmsg = "PV above OVP";
               break;

    case 302:  errmsg = "PV below UVL";
               break;

    case 304:  errmsg = "OVP below PV";
               break;

    case 306:  errmsg = "UVL above PV";
               break;

    case 307:  errmsg = "On during fault";
               break;

    case 320:  errmsg = "Fault shutdown";
               break;

    case 321:  errmsg = "AC fault shutdown";
               break;

    case 322:  errmsg = "Over-Temperature";
               break;

    case 323:  errmsg = "Fold-Back shutdown";
               break;

    case 324:  errmsg = "Over-Voltage shutdown";
               break;

    case 325:  errmsg = "Analog shut-off shutdown";
               break;

    case 326:  errmsg = "Output-Off shutdown";
               break;

    case 327:  errmsg = "Enable Open shutdown";
               break;

    case 340:  errmsg = "Internal message fault";
               break;

    case 341:  errmsg = "Input overflow";
               break;

    case 342:  errmsg = "Internal overflow";
               break;

    case 343:  errmsg = "Internal timeout";
               break;

    case 344:  errmsg = "Internal checksum";
               break;

    case 345:  errmsg = "Internal checksum error";
               break;

    case 399:  errmsg = "Unknown Error";
               break;
  }

  if (errmsg)
  {
    strncpy(prec->vala, errmsg, MAX_STRING_SIZE);
    ((char*)(prec->vala))[MAX_STRING_SIZE - 1] = '\0';
  }
  else
    *(char*)(prec->vala) = '\0';

  return 0;
}

epicsRegisterFunction(tdklambdaz_get_error_msg);
